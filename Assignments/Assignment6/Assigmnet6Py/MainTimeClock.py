from TimeStampFunctions import *

""" 
	This is a script to find the total hours worked for employees in a directory
	To Run:
	[user@machine directory]$ python MainTimeClock.py <Path of Desired Directory>
"""
# ========================================
# PREPARE HEADER FOR TEX FILE
# ========================================
File = open("HourTotals.tex","w")
File.write("\\documentclass[12pt]{article} \n"
           +  "\\usepackage{geometry} \n"
           +  "\\begin{document} \n"
           +  "\\thispagestyle{empty} \n" 
	   +  "\\begin{center} \n\n")
File.close()

# =========================================
# READ IN COMMAND LINE ARGUMENTS
# =========================================

if len(sys.argv) != 2:
   print("To Run: python MainTimeClock.py <Path of Desired Directory>")
   sys.exit()
else:
   print("Finding Directory")

# =========================================
# TEST IF DIRECTORY EXISTS
# =========================================

if os.path.isdir(sys.argv[1]):
     pass
else:
       print("Error: Could not find Directory")
       sys.exit()

# ==========================================
# GRAB A LIST OF ALL FILES
# ==========================================

MyFiles = os.listdir(sys.argv[1])

# ==========================================
# Loop over Files
# ==========================================
count = 0
for i in MyFiles:
     dataFile = MyFiles[count]
     count += 1 
     # ===========================================
     # OPEN FILES
     # ===========================================

     TimeData = open(str(sys.argv[1] + dataFile),"r")

     # ===========================================
     # READ DATA FROM FILE LINE BY LINE
     # ===========================================

     print "Reading from a file " + dataFile

     line   = TimeData.read()
     lines  = line.split("\n")
     name   = lines[0]
     TLine1 = lines[1]
     TLine2 = lines[2]
     TLine3 = lines[3]
     TLine4 = lines[4]
     TLine5 = lines[5]
     TLine6 = lines[6]
     TLine7 = lines[7] 
     CurrentStamp = TimeStamp(name, TLine1, TLine2, TLine3, TLine4, TLine5, TLine6, TLine7)

     # ===========================================
     # WRITE OUT THE FORM LETTERS
     # ===========================================

     Stamp = DocumentText(CurrentStamp)
     Stamp.WriteTotal()

# =======================================
# PREPARE FOOTER FOR TEX FILE
# =======================================

File = open("HourTotals.tex","a")
File.write("\\end{center} \n")
File.write("\\end{document} \n")
File.close()

