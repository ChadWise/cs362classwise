import sys
import os
import math
from string import whitespace

"""
   This is my library of functions that are called in the Time Clock Main.
"""

#===============
#Define a Class
#==============

class TimeStamp:
    def __init__(self, name, TLine1,TLine2,TLine3,TLine4,TLine5,TLine6,TLine7):
	self.name = name
	self.TLine1 = TLine1
        self.TLine2 = TLine2
        self.TLine3 = TLine3
        self.TLine4 = TLine4
        self.TLine5 = TLine5
        self.TLine6 = TLine6
        self.TLine7 = TLine7

    def PrintFormattedName(self):
	
	return str(self.name.replace(" ", "_"))

    def TotalTimeStamp(self):
	MyTimeStamps = [self.TLine1,self.TLine2,self.TLine3,self.TLine4,self.TLine5,self.TLine6,self.TLine7]
	#Adds monday - sunday times into an array to be looped over
	total = 0
	for i in MyTimeStamps:
		count = int(i[10]) * 60
		total += count
		count = int(i[14:16])
		total += count

	hr = total // 60 
	min = total % hr
	return (str(hr) + " hr " + str(min) + " min") 

class DocumentText:

    def __init__(self, TimeStamp):
        self.TimeStamp = TimeStamp
 
	self.center  = ("Name: " + TimeStamp.name + " " + "\\linebreak" "\n"
			+ "------------------------------- \\linebreak \n "
	                + "Total hours worked: " + TimeStamp.TotalTimeStamp() + "\\linebreak \\linebreak \n")


    def WriteTotal(self):
	FileOutName = "HourTotals.tex"
	TotalFile   = open(FileOutName,"a")
	TotalFile.write(self.center)
	TotalFile.close()
